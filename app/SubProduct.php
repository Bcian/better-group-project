<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubProduct extends Model
{
    protected $table='subproducts';
    const ACTIVE = 1;
    const DEACTIVATE = 0;
    public static $category_id=array('head_protection'=>'HEAD PROTECTION',
        'protective_eyewear'=>'PROTECTIVE EYE WEAR',
        'hearing_protection'=>'HEARING PROTECTION',
        'respiratory_protection'=>'RESPIRATORY PROTECTION',
        'hand_protection'=>'HAND PROTECTION',
        'protective_clothing'=>'PROTECTIVE CLOTHING / WORK WARE',
        'protective_footwear'=>'PROTECTIVE FOOTWEAR',
        'high_visibility'=>'HIGH-VISIBILITY(HV) CLOTHING',
        'offshore_marinesafety'=>'OFFSHORE&MARINE SAFETY',
        'foul_weather'=>'FOUL WEATHER',
        'fall_protection'=>'FALL PROTECTION',
        'spill_control'=>'SPILL CONTROL EQUIPMENTS',
        'welding_safety'=>'WELDING SAFETY',
        'first_aid'=>'FIRST AID',
        'road_safety'=>'ROAD SAFETY',
        'security_safety'=>'SECURITY SAFETY',
        'lockout_products'=>'LOCKOUT PRODUCTS',
        'fire_safety'=>'FIRE SAFETY',
        'scaff_tags'=>'SCAFF TAGS AND SCAFFOLDING ACCESSORIES',
        'safety_signs'=>'SAFETY SIGNS',
        'security_seals'=>'SECURITY SEALS',
        'electronic_test'=>'ELECTRONIC TEST TOOLS & GAS DETECTORS',
        );
    public static function slugMaker($string)
    {

        $str = str_replace(' ', '-', $string);
        $str = str_replace(',', '-', $str);
        $str = str_replace('?', '-', $str);
        $str = str_replace('(', '-', $str);
        $str = str_replace(')', '-', $str);
        $str = str_replace('/', '-', $str);
        $data = SubProduct::where('slug', $str)->get();
//        if (count($data) > 0) {
//            $str = $str . '-' . count($data);
//        }
        return $str;
    }

    //use SoftDeletes;
    public static $rule=[
        'category_id'=>'required','image'=>'required'];
    public static $update_rule=[
        'category_id'=>'required','image'=>'required'];
}
