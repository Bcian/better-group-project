<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('theme.pages.findus');
    }

    public function Add(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, Contact::$rules);
        if ($validator->passes()) {
            $contact = new Contact();
            $contact->fname = $input['fname'];
            $contact->lname = $input['lname'];
            $contact->email = $input['email'];
            $contact->phone = $input['phone'];
            $contact->subject = $input['subject'];
            $contact->message = $input['message'];
            $contact->save();
            $data['status'] = true;
            $data['message'] = 'Successful';
            return $data;
        }
        else {
            $data['status'] = false;
            $data['message'] = 'Validation Error';
            $data['message'] = $validator->errors()->toArray();
            return $data;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
