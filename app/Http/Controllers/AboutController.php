<?php

namespace App\Http\Controllers;

use App\SubProduct;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        }

    public function Aboutus()
    {
        return view('theme.pages.about');
    }
    public function Whatisnew()
    {
        return view('theme.pages.new');
    }
    public function Career()
    {
        return view('theme.pages.career');
    }
    public function Product()
    {
        return view('theme.pages.product.index');
    }
    public function Productlist($id)
    {
        switch ($id){
            case 'head_protection':
                $product_name='HEAD PROTECTION';
                $product=SubProduct::where('category_id','=','head_protection') ->get();
                break;
            case 'protective_eyewear':
                $product_name='PROTECTIVE EYEWEAR';
                $product=SubProduct::where('category_id','=','protective_eyewear') ->get();
                break;
            case 'hearing_protection':
                $product_name='HEARING PROTECTION';
                $product=SubProduct::where('category_id','=','hearing_protection') ->get();
                break;
            case 'respiratory_protection':
                $product_name='RESPIRATORY PROTECTION';
                $product=SubProduct::where('category_id','=','respiratory_protection') ->get();
                break;
            case 'hand_protection':
                $product_name='HAND PROTECTION';
                $product=SubProduct::where('category_id','=','hand_protection') ->get();
                break;
            case 'protective_clothing':
                $product_name='PROTECTIVE CLOTHING';
                $product=SubProduct::where('category_id','=','protective_clothing') ->get();
                break;
            case 'security_safety':
                $product_name='SECURITY SAFETY';
                $product=SubProduct::where('category_id','=','security_safety') ->get();
                break;
            case 'high_visibility':
                $product_name='HIGH VISIBILITY CLOTHING';
                $product=SubProduct::where('category_id','=','high_visibility') ->get();
                break;
            case 'offshore_marinesafety':
                $product_name='OFFSHORE/MARINE SAFETY';
                $product=SubProduct::where('category_id','=','offshore_marinesafety') ->get();
                break;
            case 'foul_weather':
                $product_name='FOUL WEATHER CLOTHING';
                $product=SubProduct::where('category_id','=','foul_weather') ->get();
                break;
            case 'fall_protection':
                $product_name='FALL PROTECTION';
                $product=SubProduct::where('category_id','=','fall_protection') ->get();
                break;
            case 'spill_control':
                $product_name='SPILL CONTROL EQUIPMENT';
                $product=SubProduct::where('category_id','=','spill_control') ->get();
                break;
            case 'welding_safety':
                $product_name='WELDING SAFETY';
                $product=SubProduct::where('category_id','=','welding_safety') ->get();
                break;
            case 'road_safety':
                $product_name='ROAD SAFETY';
                $product=SubProduct::where('category_id','=','road_safety') ->get();
                break;
            case 'first_aid':
                $product_name='FIRST AID';
                $product=SubProduct::where('category_id','=','first_Aid') ->get();
                break;
            case 'protective_footwear':
                $product_name='SAFETY FOOTWEAR';
                $product=SubProduct::where('category_id','=','protective_footwear') ->get();
                break;
            case 'fire_safety':
                $product_name='FIRE SAFETY';
                $product=SubProduct::where('category_id','=','fire_safety') ->get();
                break;
            case 'safety_signs':
                $product_name='SAFETY SIGNS';
                $product=SubProduct::where('category_id','=','safety_signs') ->get();
                break;
            case 'security_seals':
                $product_name='SECURITY SEALS';
                $product=SubProduct::where('category_id','=','security_seals') ->get();
                break;
            case 'lockout_products':
                $product_name='LOCKOUT TAGOUT PRODUCTS';
                $product=SubProduct::where('category_id','=','lockout_products') ->get();
                break;
            case 'scaff_tags':
                $product_name='SCAFF TAGS AND SCAFFOLDING ACCESSORIES';
                $product=SubProduct::where('category_id','=','scaff_tags') ->get();
                break;
            case 'electronic_test':
                $product_name='ELECTRONIC TEST TOOLS & GAS DETECTORS';
                $product=SubProduct::where('category_id','=','electronic_test') ->get();
                break;
            default :
                return view('theme.pages.product.index');

        }
        return view('theme.pages.product.show')
            ->with('product_name',$product_name)
            ->with('product',$product);
    }
    public function Contact()
    {
        return view('theme.pages.product.show');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
