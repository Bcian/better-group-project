<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

/*Route::get('/welcome', function () {
    return view('frontend.pages.welcome');
});*/
Route::get('/', function () {
    return view('theme.pages.home');
});
Route::get('/about','AboutController@Aboutus');
Route::get('/new','AboutController@Whatisnew');
Route::get('/career','AboutController@Career');
Route::get('/product','AboutController@Product');
Route::get('/productlist/{id}','AboutController@Productlist');
Route::get('/contact-us', 'ContactController@index');
Route::post('/save-contact', 'ContactController@Add');