<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    const ACTIVE=1;
    public static $rules = [
        'fname' => 'Required',
        'lname' => 'Required',
        'email' => 'Required|email',
        'phone' => 'Required|numeric',
        'subject' => 'Required',
        'message' => 'Required'
    ];
}
