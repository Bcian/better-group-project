<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    const ACTIVE = 1;
    const DEACTIVATE = 0;

    //use SoftDeletes;
    public static $rule=[
        'product'=>'required','image'=>'required'];
    public static $update_rule=[
        'product'=>'required','image'=>'required'];
}
