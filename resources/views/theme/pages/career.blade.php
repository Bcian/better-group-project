@extends('theme.layout.master')
@section('title', config('settings.project-name').'| career')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="banner-wrap about-banner white">
                <div class="row m-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="head bold f-36">
                                    <span class="light">Start your Career with</span> us
                                </p>
                                {{--<p class="desc light f-18">--}}
                                    {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.--}}
                                {{--</p>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-grey">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-60 mt-40">
                    <div class="row">
                        <div class="col-sm-6 mt-20">
                            <p class="text-black f-22 bold">Quality Requirements</p>
                            <P class="light text-black f-16">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis sint ipsa veritatis labore illo
                                aspernatur. Doloremque et est fuga nesciunt blanditiis doloribus, omnis nulla eaque fugit
                                eum quae optio, maiores officia, veritatis quidem ipsum dolore eveniet. Quis laboriosam temporibus
                                voluptate.
                            </P>
                            <P class="light text-black f-16">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis sint ipsa veritatis labore illo
                                aspernatur.
                            </P>

                            {{--<a class="btn btn-default btn-blue mt-20" href="#" role="button">KNOW MORE</a>--}}

                        </div>
                        <div class="col-sm-6 mt-20">
                            <img src="/theme/img/career.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <p class="bold f-22 text-center text-black mt-60 mb-60">Open positions</p>
        </div>
        <div class="col-xs-12 mb-60">
            <div class="row">
                <div class="col-xs-10 col-md-offset-1  mt-10 mb-10">
                    <div class="position-wrap">
                        <div class="accrd-head">
                            <p class="bold f-18 text-black">Marketing Executive
                                <i class="glyphicon glyphicon-menu-right down"></i>
                            </p>
                        </div>
                        <div class="accrd-body block">
                            <p class="mb-0">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus aut obcaecati doloribus expedita iusto distinctio molestias
                                tempora voluptatum nisi corporis?
                            </p>
                            <a href="" class="btn btn-default btn-blue mt-20" role="button">Apply</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-md-offset-1  mt-10 mb-10">
                    <div class="position-wrap">
                        <div class="accrd-head">
                            <p class="bold f-18 text-black">Marketing Executive
                                <i class="glyphicon glyphicon-menu-right"></i>
                            </p>
                        </div>
                        <div class="accrd-body">
                            <p class="mb-0">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus aut obcaecati doloribus expedita iusto distinctio molestias
                                tempora voluptatum nisi corporis?
                            </p>
                            <a href="" class="btn btn-default btn-blue mt-20" role="button">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection