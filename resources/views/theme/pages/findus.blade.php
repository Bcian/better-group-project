@extends('theme.layout.master')
@section('title', config('settings.project-name').'| find us')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="banner-wrap about-banner white">
                <div class="row m-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="head bold f-36">
                                    <span class="light">Get in </span> Touch
                                    <span class="light">With Us </span>
                                {{--<p class="desc light f-18">--}}
                                    {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.--}}
                                {{--</p>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 mt-60 mb-60">
            <div class="row">
                <div class="col-sm-6">
                    <p class="bold f-18 text-black">Contact us</p>
                    {{--<p class="light f-16 text-black">Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi officia nostrum ad tenetur, inventore asperiores--}}
                        {{--debitis eveniet minima! Dolor aliquid officiis accusantium molestias veritatis cum ad ex qui quisquam--}}
                        {{--perferendis.--}}
                    {{--</p>--}}
                    <form action="" class="contact-form mt-40" id="contact-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::text('fname','',array('class' => 'form-control','placeholder'=>'First name ','id'=>'email'))!!}
                                    <div id="fname-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::text('lname','',array('class' => 'form-control','placeholder'=>'Last name ','id'=>'email'))!!}
                                    <div id="lname-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::email('email','',array('class' => 'form-control','placeholder'=>'Your email ','id'=>'email'))!!}
                                    <div id="email-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::text('phone','',array('class' => 'form-control','placeholder'=>'Your Phone no: ','id'=>'email'))!!}
                                    <div id="phone-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::text('subject','',array('class' => 'form-control','placeholder'=>'Subject ','id'=>'email'))!!}
                                    <div id="subject-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Your Name">--}}
                                    {!! Form::textarea('message','',array('class' => 'form-control','placeholder'=>'Write message ','id'=>'email','rows'=>"6"))!!}
                                    <div id="message-error" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group" style="height:40px;">
                                    <button type="submit" id="submit-id" class="btn btn-default btn-blue">Send Message</button>
                                    <div id="send-error" class="text-danger"></div>
                                    {{--<input type="submit" value="Send " id="submit-id" class="btn btn-default">--}}
                                </div>
                            </div>
                        </div>
                    </form>

            </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d230885.99038348577!2d51.37156529934528!3d25.284253097162615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c534ffdce87f%3A0x44d9319f78cfd4b1!2sDoha%2C+Qatar!5e0!3m2!1sen!2sin!4v1515576389373"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="col-sm-8 contact-block mt-10">
                            <table class="table">
                                <tr>
                                    <!-- <td>
                                                <i class="icon-spill"></i>
                                            </td> -->
                                    <td>
                                        <p class="bold">Office Address</p>
                                        <p class="light">
                                            Better Safety Trading Contracting & Decoration ,Shop 080, Block 25. Sayer, Barwa Commercial Avenue, Doha, Qatar , P.O Box.
                                            213001
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-8 contact-block mt-10">
                            <table class="table">
                                <tr>
                                    <!-- <td>
                                                <i class="icon-spill"></i>
                                            </td> -->
                                    <td>
                                        <p class="bold">Contact info</p>
                                        <p class="light">
                                            Tel: +974 44722499
                                            <br> Fax: 44722499,

                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-8 contact-block mt-10">
                            <table class="table">
                                <tr>
                                    <!-- <td>
                                                <i class="icon-spill"></i>
                                            </td> -->
                                    <td>
                                        <p class="bold">Email</p>
                                        <p class="light">
                                            info@bettergroupsafety.com
                                            <br> sales@bettergroupsafety.com
                                        </p>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection