@extends('theme.layout.master')
@section('title', config('settings.project-name').'| New')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="banner-wrap about-banner white">
                <div class="row m-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="head bold f-36">
                                    <span class="light">What's new</span>.
                                </p>
                                {{--<p class="desc light f-18">--}}
                                    {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.--}}
                                {{--</p>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row ">
        <div class="col-xs-12 mt-80 mb-80">
            <div class="row new-wrap">
                <div class="col-sm-4 col-md-3">
                    <a href="#">
                        <div class="new-item text-black">
                            <img src="/theme/img/new-1.jpg" alt="" class="img-responsive">
                            <div class="content">
                                <p class="f-18 bold">Head Protection</p>
                                <p class="f-12 light mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum sunt laudantium doloremque
                                    magnam accusamus aut repudiandae tempora minima quia exercitationem.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-4 col-md-3">
                    <a href="#">
                        <div class="new-item text-black">
                            <img src="/theme/img/new-2.jpg" alt="" class="img-responsive">
                            <div class="content">
                                <p class="f-18 bold">Head Protection</p>
                                <p class="f-12 light mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum sunt laudantium doloremque
                                    magnam accusamus aut repudiandae tempora minima quia exercitationem.</p>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="col-sm-4 col-md-3">
                    <a href="#">
                        <div class="new-item text-black">
                            <img src="/theme/img/new-3.jpg" alt="" class="img-responsive">
                            <div class="content">
                                <p class="f-18 bold">Head Protection</p>
                                <p class="f-12 light mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum sunt laudantium doloremque
                                    magnam accusamus aut repudiandae tempora minima quia exercitationem.</p>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="col-sm-4 col-md-3">
                    <a href="#">
                        <div class="new-item text-black">
                            <img src="/theme/img/new-4.jpg" alt="" class="img-responsive">
                            <div class="content">
                                <p class="f-18 bold">Head Protection</p>
                                <p class="f-12 light mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum sunt laudantium doloremque
                                    magnam accusamus aut repudiandae tempora minima quia exercitationem.</p>
                            </div>
                        </div>
                    </a>

                </div>
            </div>
        </div>

    </div>
</div>
    @endsection