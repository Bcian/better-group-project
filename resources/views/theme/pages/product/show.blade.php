@extends('theme.layout.master')
@section('title', config('settings.project-name').'| productlist')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 no-padding">
                <div class="banner-wrap about-banner white">
                    <div class="row m-0">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <p class="head bold f-36">
                                        <span class="light">We Have The</span> Best Quality
                                    </p>
                                    <p class="desc light f-18">
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="link-nav">
                <p>
                <span>
                    <a href="">
                        Home
                    </a>
                </span>
                    <span>
                    <a href="/product">
                        Product
                    </a>
                </span>
                    <span class="active">
                    <a href="">
                        {{$product_name}}
                    </a>
                </span>
                </p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row product">
            <div class="col-xs-12">
                <p class="light f-36 text-center mt-40 mb-40">{{$product_name}}</p>
            </div>
            <div class="col-xs-12 mb-80">
                <div class="row">
                    @foreach($product as $key=>$product)
                        <div class="col-sm-4 mt-20" data-toggle="modal" data-target="#myModal{{$key}}">
                            <div class="item">
                                <img src="{{url($product['image'])}}" alt="" class="img-responsive">
                            </div>
                            <p class="text-center f-18 mt-20">{{$product->name}}</p>
                        </div>
                        <div class="modal fade" id="myModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4> -->
                                    </div>
                                    <div class="modal-body">
                                        {{--                                    @if(!isEmptyOrNullString($product))--}}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{url($product['image'])}}" alt="" class="img-responsive">
                                            </div>
                                            <div class="col-md-6">
                                                <p class="blue f-18 bold">{{$product->name}}</p>
                                                <p class="text-black light">{{$product->description}} </div>
                                        </div>
                                        {{--@endif--}}
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--<div class="col-sm-4 mt-20">--}}
                    {{--<div class="item">--}}
                    {{--<img src="/theme/img/head.jpg" alt="" class="img-responsive">--}}
                    {{--</div>--}}
                    {{--<p class="text-center f-18 mt-20">Head protection</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 mt-20">--}}
                    {{--<div class="item">--}}
                    {{--<img src="/theme/img/hand.jpg" alt="" class="img-responsive">--}}
                    {{--</div>--}}
                    {{--<p class="text-center f-18 mt-20">hand protection</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 mt-20">--}}
                    {{--<div class="item">--}}
                    {{--<img src="/theme/img/head.jpg" alt="" class="img-responsive">--}}
                    {{--</div>--}}
                    {{--<p class="text-center f-18 mt-20">Head protection</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 mt-20">--}}
                    {{--<div class="item">--}}
                    {{--<img src="/theme/img/hand.jpg" alt="" class="img-responsive">--}}
                    {{--</div>--}}
                    {{--<p class="text-center f-18 mt-20">hand protection</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 mt-20">--}}
                    {{--<div class="item">--}}
                    {{--<img src="/theme/img/head.jpg" alt="" class="img-responsive">--}}
                    {{--</div>--}}
                    {{--<p class="text-center f-18 mt-20">Head protection</p>--}}
                    {{--</div>--}}
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->

@endsection