@extends('theme.layout.master')
@section('title', config('settings.project-name').'| product')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 no-padding">
            <div class="banner-wrap about-banner white">
                <div class="row m-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="head bold f-36">
                                    <span class="light">We Have The</span> Best Quality
                                </p>
                                {{--<p class="desc light f-18">--}}
                                    {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.--}}
                                {{--</p>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row product">
        <div class="col-xs-12">
            <p class="bold f-36 text-center mt-40 mb-40">Our Products</p>
        </div>
        <div class="col-xs-12 mb-80">
            <div class="row">
                <div class="col-sm-2 mt-20">
                    <a href="{{url('/productlist/head_protection')}}">
                        <div class="item main text-center">
                            <i class="icon-head"></i>
                            <p>head protection</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/protective_eyewear">
                        <div class="item main text-center">
                            <i class="icon-eye-wear"></i>
                            <p>protective eye wear</p>
                        </div>
                    </a>

                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/hearing_protection">
                        <div class="item main text-center">
                            <i class="icon-hearing"></i>
                            <p>hearing protection</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/respiratory_protection">
                        <div class="item main text-center">
                            <i class="icon-respiratory"></i>
                            <p>respiratory protection</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/hand_protection">
                        <div class="item main text-center">
                            <i class="icon-hand"></i>
                            <p>hand protection</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/protective_footwear">
                        <div class="item main text-center">
                            <i class="icon-safty-footwear"></i>
                            <p>safety footwear</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/spill_control">
                        <div class="item main text-center">
                            <i class="icon-spill-control"></i>
                            <p>spill control equipment</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/fall_protection">
                        <div class="item main text-center">
                            <i class="icon-fall-protection"></i>
                            <p>fall protection</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/foul_weather">
                        <div class="item main text-center">
                            <i class="icon-foul-weather"></i>
                            <p>foul weather clothing</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/offshore_marinesafety">
                        <div class="item main text-center">
                            <i class="icon-offshore"></i>
                            <p>OFFSHORE/MARINE SAFETY</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/protective_clothing">
                        <div class="item main text-center">
                            <i class="icon-protective-clothing"></i>
                            <p>PROTECTIVE CLOTHING</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/high_visibility">
                        <div class="item main text-center">
                            <i class="icon-high-visibility-clothing"></i>
                            <p>HIGH VISIBILITY CLOTHING</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/security_safety">
                        <div class="item main text-center">
                            <i class="icon-security-safety"></i>
                            <p>SECURITY SAFETY</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/first_aid">
                        <div class="item main text-center">
                            <i class="icon-first-aid"></i>
                            <p>first aid</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/welding_safety">
                        <div class="item main text-center">
                            <i class="icon-welding-safety"></i>
                            <p>welding safety</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/road_safety">
                        <div class="item main text-center">
                            <i class="icon-road-safety"></i>
                            <p>road safety</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/fire_safety">
                        <div class="item main text-center">
                            <i class="icon-fire-safety"></i>
                            <p>fire safety</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/safety_signs">
                        <div class="item main text-center">
                            <i class="icon-safety-signs"></i>
                            <p>safety signs</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/security_seals">
                        <div class="item main text-center">
                            <i class="icon-security-seal"></i>
                            <p>security seals</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/lockout_products">
                        <div class="item main text-center">
                            <i class="icon-lockout-tagout"></i>
                            <p>Lockout Tagout Products</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2 mt-20">
                    <a href="/productlist/scaff_tags">
                        <div class="item main text-center">
                            <i class="icon-scaff-tag"></i>
                            <p>scaff tags and scaffolding accessories</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-2">
                    <a href="/productlist/electronic_test">
                        <div class="item main text-center">
                            <i class="icon-gas-detector"></i>
                            <p>electronic test tools &gas detectors</p>
                        </div>
                    </a>
                </div>
                {{--<div class="col-sm-2 mt-20">--}}
                    {{--<a href="/productlist/electronic_test">--}}
                        {{--<div class="item main text-center">--}}
                            {{--<i class="icon-gas-detector"></i>--}}
                            {{--<p>electronic test tools &gas detectors</p>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
                <div class="col-sm-2"></div>
            </div>
        </div>

    </div>
</div>
    @endsection