@extends('theme.layout.master')
@section('title', config('settings.project-name').'| About')
@section('meta')
    @parent
@stop
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 no-padding">
                <div class="banner-wrap about-banner white">
                    <div class="row m-0">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <p class="head bold f-36">
                                        <span class="light">About</span> us
                                    </p>
                                    {{--<p class="desc light f-18">--}}
                                        {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nisi.--}}
                                    {{--</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 mb-60 mt-40">
                <div class="row">
                    <div class="col-sm-6 mt-20">
                        <img src="/theme/img/about-desc.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-sm-6 mt-20">
                        <p class="text-black f-22 bold">ABOUT US</p>
                        <P class="light text-black f-16">Welcome to Better Group, providing Personal Protective Equipment (PPE) and other industrial safety equipment to help you meet today’s high health and safety standards. As leading PPE suppliers we are proud to be able to offer the highest quality workplace safety equipment and PPE industrial supplies to various industries & Government sectors in the Middle East& Africa
                            Health, Safety and Environment is responsible for monitoring environmental conditions and correcting hazards as they are identified, as well as for assessing and placing individuals in environments they are capable of working without harm to themselves or others.   HSE is responsible for paying all workers, we are here to reduce their responsibility to provide them right PPE on right time ,  All of our products meet all international regulations and standards like ANSI,CE,EN etc..!
                            we are pleased to offer you PPE and other industrial safety equipment supplies from many of the leading manufacturers of industrial PPE, including  Bluefox, Wolf,3M, Armer Dupont Tyek,Traffi Safe, Univet, Peltor, Wanco, and  – to mention just a few.Our efficient and highly trained staffs are on hand to assist and ensure any customer technical queries are answered.  on-time deliveries throughout GCC are met from our 21,000 sq ft stockholding facility in Qatar / UAE.
                            <P class="light text-black f-16">Thank you for choosing Better Group – you’re in safe hands.   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 no-padding">
                <div id="about-carousel" class="carousel slide" data-interval="false" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#about-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#about-carousel" data-slide-to="1"></li>
                        <li data-target="#about-carousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <p class="f-36 they-head bold">They Say</p>
                        <div class="item active" style="background-image:url(/theme/img/about-slider-1.jpg)">
                            <div class="carousel-caption">
                                <p class="desc mb-40">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint itaque eius cum non vel dolorem
                                    quia voluptatum consequatur neque quis.</p>
                                <ul class="list-inine list-unstyled">
                                    <li>
                                        <img src="/theme/img/user-dummy.png" alt="" class="img-responsive">
                                    </li>
                                    <li class="f-18">
                                        <p class="bold">John Doe</p>
                                        <p class="light"> United Kingdom</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item " style="background-image:url(/theme/img/about-slider-1.jpg)">
                            <div class="carousel-caption">
                                <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint itaque eius cum non vel dolorem
                                    quia voluptatum consequatur neque quis.</p>
                                <ul class="list-inine list-unstyled">
                                    <li>
                                        <img src="/theme/img/user-dummy.png" alt="" class="img-responsive">
                                    </li>
                                    <li>
                                        <p>John Doe</p>
                                        <p> United Kingdom</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="item " style="background-image:url(/theme/img/about-slider-1.jpg)">
                            <div class="carousel-caption">
                                <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint itaque eius cum non vel dolorem
                                    quia voluptatum consequatur neque quis.</p>
                                <ul class="list-inine list-unstyled">
                                    <li>
                                        <img src="/theme/img/user-dummy.png" alt="" class="img-responsive">
                                    </li>
                                    <li>
                                        <p>John Doe</p>
                                        <p> United Kingdom</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 about-red text-center">
                <img src="/theme/img/vision.svg" alt="" class="img-responsive">
                <p class="mt-20">our vision</p>
                <p> Workplace Safety, aspires to set the standard in occupational safety, industrial hygiene, and workplace risk management, To be a recognized leader promoting healthy and safe workplaces in GCC based on tripartism, participation and the development of an OSH risk prevention culture, to ensure a smart, sustainable, productive and inclusive economy.
                </p>
            </div>
            <div class="col-sm-6 about-red text-center">
                <img src="/theme/img/mision.svg" alt="" class="img-responsive">
                <p class="mt-20">our mission</p>
                <p>
                    Our mission is to provide our clients with premier occupational safety and industrial safety Product& services designed to reduce workplace injuries and illnesses, which promotes client Profitability.</p>
            </div>
            {{--<div class="col-sm-4 about-red text-center">--}}
                {{--<img src="/theme/img/goal.svg" alt="" class="img-responsive">--}}
                {{--<p class="mt-20">our goals</p>--}}
                {{--<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam adipisci deserunt facilis deleniti corporis, dignissimos--}}
                    {{--est, fuga quos, molestiae facere eligendi impedit cumque quas nisi accusantium expedita recusandae magnam--}}
                    {{--delectus natus laboriosam.</p>--}}
            {{--</div>--}}
        </div>
    </div>
    @endsection