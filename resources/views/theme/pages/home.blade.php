@extends('theme.layout.master') @section('title', config('settings.project-name').'| Home') @section('meta') @parent @stop
@section('style') @endsection @section('content')
<section class="slider-wrapper">

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 no-padding">
                <div id="home-carousel" class="carousel slide" data-pause="null" data-interval="2000" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#home-carousel" data-slide-to="1"></li>
                        <!-- <li data-target="#home-carousel" data-slide-to="2"></li> -->
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active" style="background-image:url(/theme/img/better-slider-1.jpg)">
                            <div class="wrap">
                                <div class="carousel-caption">
                                    <p class="f-36 light blue-box">Better
                                        <span class="bold">safe</span> than
                                        <span class="bold">sorry</span>
                                    </p>
                                    <p class="bold f-22 head-2">Safety &amp; Health are vital to human life.
                                        <br> But it is not that complicated.</p>
                                    <p class="f-18 light head-3">
                                        <span class="bold">Better Group,</span> through its considerable expertise in the field of safety and
                                        security, has been able to research and develop customized safety equipments for
                                        workplace safety.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="background-image:url(/theme/img/better-slider-2.jpg)">
                            <div class="wrap">
                                <div class="carousel-caption">
                                    <p class="f-36 light blue-box">Better
                                        <span class="bold">safe</span> than
                                        <span class="bold">sorry</span>
                                    </p>
                                    <p class="bold f-22 head-2">Safety &amp; Health are vital to human life.
                                        <br> But it is not that complicated.</p>
                                    <p class="f-18 light head-3">
                                        <span class="bold">Better Group,</span> through its considerable expertise in the field of safety and
                                        security, has been able to research and develop customized safety equipments for
                                        workplace safety.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <!-- <a class="left carousel-control" href="#home-carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#home-carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 mb-80 mt-80">
                <div class="row">
                    <div class="col-sm-6 text-black">
                        <div class="border-wrap ">
                            <div class="block">
                                <span class="top"></span>
                            </div>
                            <p class="f-22 bold">There are numerous risks at worksites.
                                <br> But there are innumerous ways to
                                <br> protect too!</p>
                            <p class="light f-16">
                                Managing health and safety does not have to be complicated, costly or time consuming. In fact it’s easier than we imagine.
                                The employers and the employees at the work place can be carefully protected with adequate
                                safety measures. Taking responsible steps to prevent accidents or injuries from happening
                                is the est gift you can offer your employees and their families.
                            </p>
                            <div class="block">
                                <span class="btm"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <img src="{{ url('/theme/img/better-about.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid bg-grey">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 mb-80 mt-80">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 text-black">
                                <div class="border-wrap ">
                                    <p class="f-22 bold">Better group personal protection
                                        <br> solutions
                                    </p>
                                    <p class="light f-16 mr">
                                        Better Group Personal Protection Solutions are available for varied situations and different reasons. The reasons being the
                                        Possible Hazards such as Harmful or Hazardous Temperatures and Humidity, Hot Splashes
                                        from Molten Metal and Other Hot Liquids, Impacts from Tools, Machinery, and Materials,
                                        Hazardous Chemicals, Ionizing Radiation or even Moving
                                    </p>
                                    <div class="block">
                                        <!-- <span class="btm"></span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 b-items">
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-head"></i>
                                        <p>head protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-eye-wear"></i>
                                        <p>protective eye wear</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-hearing"></i>
                                        <p>hearing protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-respiratory"></i>
                                        <p>respiratory protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-hand"></i>
                                        <p>hand protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center">
                                        <i class="icon-safty-footwear"></i>
                                        <p>safety footwear</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-spill-control"></i>
                                        <p>spill control equipment</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-fall-protection"></i>
                                        <p>fall protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-foul-weather"></i>
                                        <p>foul weather clothing</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-offshore"></i>
                                        <p>offshore/marine safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-protective-clothing"></i>
                                        <p>protective clothing</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-high-visibility-clothing"></i>
                                        <p>high visibility clothing</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content  text-center visible-xs visible-sm">
                                        <i class="icon-security-safety"></i>
                                        <p>security safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center visible-xs visible-sm">
                                        <i class="icon-first-aid"></i>
                                        <p>first aid</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center visible-xs visible-sm">
                                        <i class="icon-welding"></i>
                                        <p>welding safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center visible-xs visible-sm">
                                        <i class="icon-road-safety"></i>
                                        <p>road safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-fire-safety"></i>
                                        <p>fire safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-safety-signs"></i>
                                        <p>safety signs</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-security-seal"></i>
                                        <p>security seals</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-lockout-tagout"></i>
                                        <p>Lockout Tagout Products</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-scaff-tag"></i>
                                        <p>scaff tags and scaffolding accessories</p>
                                    </div>
                                    <div class="col-sm-3 col-md-4 item-content text-center visible-xs visible-sm">
                                        <i class="icon-gas-detector"></i>
                                        <p>electronic test tools &gas detectors</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 b-items next hidden-xs hidden-sm">
                                <div class="row">
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-spill-control"></i>
                                        <p>spill control equipment</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-fall-protection"></i>
                                        <p>fall protection</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-foul-weather"></i>
                                        <p>foul weather clothing</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-offshore"></i>
                                        <p>offshore/marine safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-protective-clothing"></i>
                                        <p>protective clothing</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-high-visibility-clothing"></i>
                                        <p>high visibility clothing</p>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-12 b-items hidden-xs hidden-sm">
                                <div class="row">
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-fire-safety"></i>
                                        <p>fire safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-safety-signs"></i>
                                        <p>safety signs</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-security-seal"></i>
                                        <p>security seals</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-lockout-tagout"></i>
                                        <p>Lockout Tagout Products</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-scaff-tag"></i>
                                        <p>scaff tags and scaffolding accessories</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-gas-detector"></i>
                                        <p>electronic test tools &gas detectors</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 next b-items hidden-sm hidden-xs">
                                <div class="row">
                                    <div class="col-sm-3 col-md-2 null text-center">
                                        <!-- <i class="icon-high-visibility"></i>
                                            <p>high visibility clothing</p> -->
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content  text-center">
                                        <i class="icon-security-safety"></i>
                                        <p>security safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-first-aid"></i>
                                        <p>first aid</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-welding-safety"></i>
                                        <p>welding safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2 item-content text-center">
                                        <i class="icon-road-safety"></i>
                                        <p>road safety</p>
                                    </div>
                                    <div class="col-sm-3 col-md-2  null  text-center">
                                        <!-- <i class="icon-high-visibility"></i>
                                            <p>high visibility clothing</p> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 mt-60 mb-60">
                <p class="bold f-18 dark-grey mb-40">Brands we deal with</p>
                {{--
                <div class="row">--}}
                    <div class="owl-carousel owl-theme mt-60">
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/1.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/2.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/3.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/4.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/5.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-item">
                                <img src="{{ url('/theme/img/clients/6.jpg')}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/a.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/2a.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/c.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/d.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/e.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/f.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/g.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/h.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/i.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/j.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/k.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}} {{--
                        <div class="item">--}} {{--
                            <div class="client-item">--}} {{--
                                <img src="{{ url('/theme/img/clients/l.jpg')}}" alt="" class="img-responsive">--}} {{--
                            </div>--}} {{--
                        </div>--}}


                    </div>



                </div>
                {{--</div>--}}
        </div>
    </div>
    </div>

</section>
@endsection {{--@push('script')--}} {{--@stop--}}