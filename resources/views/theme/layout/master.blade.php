<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Keywords" content="">
    <title>Better Group
    </title>
    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="img/favicon.png">
    <link href="/theme/css/page.css" rel="stylesheet">
    <!-- bower:css -->
    <link rel="stylesheet" href="/theme/bower_components/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="{{url('/theme/js/owl/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{url('/theme/js/owl/owl.carousel.min.css')}}">
    <!-- endbower -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
    @stack('style')
</head>

<body id="body" data-base-url="{{url()}}">
<div id="pre-loader" class="pre-loader">
    <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
        <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
    </svg>
</div>
@include('theme.layout.header')

{{--Yield Content--}}
@yield('content')
@include('theme.layout.footer')
<!-- bower:js -->
<script src="{{url('/theme/bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{url('/theme/bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{url('/theme/bower_components/ev-emitter/ev-emitter.js')}}"></script>
<script src="{{url('/theme/bower_components/imagesloaded/imagesloaded.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- endbower -->
<script src="{{url('theme/js/owl/owl.carousel.min.js')}}"></script>
<script src="{{url('/theme/js/main.js')}}"></script>
@stack('script')
</body>
</html>