<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="{{url('/productlist/head_protection')}}">head protection</a>
                            </li>
                            <li>
                                <a href="/productlist/respiratory_protection">respiratory protection</a>
                            </li>
                            <li>
                                <a href="/productlist/protective_eyewear">protective eye wear</a>
                            </li>
                            <li>
                                <a href="/productlist/hearing_protection">hearing protection</a>
                            </li>
                            <li>
                                <a href="/productlist/lockout_products">lockout tagout products</a>
                            </li>
                            <li>
                                <a href="/productlist/scaff_tags">scaff tags and scaffolding accessories</a>
                            </li>

                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="/productlist/spill_control">spill control equipment</a>
                            </li>
                            <li>
                                <a href="/productlist/fall_protection">fall protection</a>
                            </li>
                            <li>
                                <a href="/productlist/foul_weather">foul weather clothing</a>
                            </li>
                            <li>
                                <a href="/productlist/offshore_marinesafety">offshore/marine safety</a>
                            </li>
                            <li>
                                <a href="/productlist/security_seals">security seals</a>
                            </li>
                            <li>
                                <a href="/productlist/electronic_test">electronic test tools & gas detectors</a>
                            </li>


                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="/productlist/hand_protection">hand protection</a>
                            </li>
                            <li>
                                <a href="/productlist/protective_footwear">safety footwear</a>
                            </li>
                            <li>
                                <a href="/productlist/protective_clothing">protective clothing</a>
                            </li>
                            <li>
                                <a href="/productlist/high_visibility">high visibility clothing</a>
                            </li>
                            <li>
                                <a href="/productlist/safety_signs">safety signs</a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="/productlist/security_safety">security safety</a>
                            </li>
                            <li>
                                <a href="/productlist/first_aid">first aid</a>
                            </li>
                            <li>
                                <a href="/productlist/welding_safety">welding safety</a>
                            </li>
                            <li>
                                <a href="/productlist/road_safety">road safety</a>
                            </li>
                            <li>
                                <a href="/productlist/fire_safety">fire safety</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>