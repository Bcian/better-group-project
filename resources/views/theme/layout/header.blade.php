<div class="container-fluid">
    <nav id="header" class="navbar navbar-fixed-top header" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed navbar-mobile" data-toggle="collapse" data-target="#mobile-view" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="/theme/img/logo.svg" class="img-responsive">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mobile-view">
                <ul class=" nav navbar-nav navbar-content" id="menu-center">
                    <li>
                        <a href="{{url('/')}}">home</a>
                    </li>
                    <li>
                        <a href="{{url('/about')}}">
                            about us
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/product')}}">
                            products
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/new')}}">
                            what's new
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/career')}}">
                            career
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/contact-us')}}">
                            find us
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div style="height:70px;"></div>
</div>