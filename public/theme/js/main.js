$(document).ready(function() {
  BASE_URL = $("body").data("base-url");
  $.ajaxSetup({
    headers: {
      "X-CSRF-Token": $('meta[name="_token"]').attr("content")
    }
  });
  $(document).imagesLoaded(function() {
    $(".pre-loader").fadeOut(1000);
  });
  $("#submit-id").click(function(event) {
    event.preventDefault();

    $(".contact-form").removeClass("text-danger");
    var formdata = $("#contact-form").serialize();

    // alert(formdata);
    $.post(BASE_URL + "/save-contact", formdata).done(function(data) {
      if (data.status == true) {
        $("#contact-form")
          .get(0)
          .reset();
        swal("Submitted", " Your query is saved", "success");
        // window.location.reload();
      } else {
        if (data.message.fname) $("#fname-error").text(data.message.fname);
        if (data.message.lname) $("#lname-error").text(data.message.lname);
        if (data.message.email) $("#email-error").text(data.message.email);
        if (data.message.message)
          $("#message-error").text(data.message.message);
        if (data.message.phone) $("#phone-error").text(data.message.phone);
        if (data.message.subject)
          $("#subject-error").text(data.message.subject);
      }
    });
  });
  //accrd

  $(".position-wrap > .accrd-head").click(function(e) {
    e.preventDefault();

    $(this)
      .siblings(".accrd-body")
      .toggle("fast", "linear");

    $(this)
      .find("i")
      .toggleClass("down");
  });
});

$(".owl-carousel").owlCarousel({
  items: 6,
  loop: !0,
  margin: 10,
  nav: !1,
  dots: !1,
  autoplay: !0,
  autoplayTimeout: 1e3,
  autoplayHoverPause: !1,
  responsiveClass: !0,
  responsive: { 0: { items: 2 }, 600: { items: 4 }, 1e3: { items: 6 } }
});
