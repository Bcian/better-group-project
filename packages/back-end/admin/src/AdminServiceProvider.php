<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:14 PM
 */
namespace BackEnd\Admin;

use Illuminate\Support\ServiceProvider;

Class AdminServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('admin', function($app){
            return new Admin;
        });
    }
    public function boot()
    {
        /*
       |----------------------------
       | ROUTES
       |----------------------------
       */
        require __DIR__.'/Http/routes.php';
        /*
       |----------------------------
       | VIEWS
       |----------------------------
       */
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'admin');

        /*
        |----------------------------
        | MIGRATIONS
        |----------------------------
        */
        $this->publishes([
            __DIR__ . '/../database/migrations'=> database_path('migrations')
        ], 'migrations');

        /*
         |----------------------------
         | PUBLIC ASSETS
         |----------------------------
         */
        $this->publishes([
            __DIR__.'/../public' => public_path('back-end/admin/'),
        ], 'public');

        /*
        |----------------------------
        | MIDDLEWARE
        |----------------------------
        */
        app()->router->middleware('admin_auth', \BackEnd\Admin\Http\Middleware\AdminAuthMiddleware::class);
        app()->router->middleware('admin_menu', \BackEnd\Admin\Http\Middleware\AdminMenuMiddleware::class);
        app()->router->middleware('admin_guest', \BackEnd\Admin\Http\Middleware\AdminRedirectIfAuthenticated::class);
    }
}
