<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 22/12/15
 * Time: 10:44 AM
 */

namespace BackEnd\Admin\Menus;

use App\Common\Roles;

class MenuFactory
{
    public static function getProcessor($role_id){
        if ($role_id == Roles::ROLE_ADMIN) {
            return new AdminRoleMenu();
        }
    }
}