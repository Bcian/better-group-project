<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 22/12/15
 * Time: 10:44 AM
 */

namespace BackEnd\Admin\Menus;

class AdminRoleMenu extends AbstractRoleMenu
{

    public function process($menu)
    {
        $main = $menu->add('MAIN NAVIGATION');
        $home = $menu->add('Dashboard',array('route' => 'admin.get_home'))->icon('dashboard');
//        $product = $menu->add('Products',array('route' => 'admin.product.index'))->icon('dashboard');
        $subproduct = $menu->add('Subproducts',array('route' => 'admin.subproduct.index'))->icon('dashboard');
        $contact = $menu->add('Contacts',array('route' => 'admin.contact.index'))->icon('dashboard');
    }
}