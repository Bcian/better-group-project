<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:28 PM
 */
namespace BackEnd\Admin\Http\Middleware;

use App\Common\Roles;
use Closure;
use Illuminate\Contracts\Auth\Guard;


class AdminRedirectIfAuthenticated
{
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check() && $this->auth->user()->role_id == Roles::ROLE_ADMIN) {

            return redirect('/admin/dashboard');
        }
        //TODO:: Redirect authenticated user

        return $next($request);
    }
}