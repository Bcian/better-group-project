<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:28 PM
 */
namespace BackEnd\Admin\Http\Middleware;

use App\Common\Roles;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminAuthMiddleware
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || (!$this->auth->guest() && $this->auth->user()->role_id != Roles::ROLE_ADMIN)) {
         if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }
         else {

             if ($this->auth->check()&&$this->auth->user()->role_id != Roles::ROLE_ADMIN) {
                 return redirect('/home');
             }
                return redirect('admin/auth/login');
            }
        }
      return $next($request);
    }
}

