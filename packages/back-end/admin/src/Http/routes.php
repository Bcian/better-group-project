<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:15 PM
 */
/* Un secure Routes | Will be available to public */
Route::group(['prefix' => 'admin', 'namespace' => 'BackEnd\Admin\Http\Controllers'], function () {
    Route::controllers(['auth' => 'AuthController']);
    Route::get('/', 'HomeController@getDashboard');
});
Route::group(['prefix' => 'admin', 'namespace' => 'BackEnd\Admin\Http\Controllers', 'middleware' => ['admin_auth','admin_menu']], function () {
    Route::get('/dashboard', array('as' => 'admin.get_home', 'uses' => 'HomeController@getDashboard'));
    Route::get('/auth/logout', array('as' => 'admin.auth.logout', 'uses' => 'AuthController@getLogout'));
    Route::get('/errors/404', array('as' => 'admin.get_404', 'uses' => 'HomeController@get404'));

    /**
     * Routes Comes here
     */

    Route::resource('product','ProductController');
    Route::get('product-info','ProductController@getProductInfo');
    Route::get('product/activate/{id}','ProductController@activate');
    Route::get('product/deactivate/{id}','ProductController@deactivate');

    Route::resource('subproduct','SubProductController');
    Route::get('subproduct-info','SubProductController@getsubProductInfo');
    Route::get('subproduct/activate/{id}','SubProductController@activate');
    Route::get('subproduct/deactivate/{id}','SubProductController@deactivate');

    Route::resource('contact','ContactController');
    Route::get('/contacts-info','ContactController@getContactInfo');

});