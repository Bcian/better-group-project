<?php

/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:28 PM
 */


namespace BackEnd\Admin\Http\Controllers;

use App\Common\Roles;
use Illuminate\Http\Request;
use Auth;
use Input;
use Hash;
use Lang;
use App\User;

class AuthController extends BaseController
{

    public function __construct()
    {
        $this->middleware('admin_guest', ['except' => 'getLogout']);
    }

    public function getLogin()
    {
        return $this->view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $credentials['role_id'] = Roles::ROLE_ADMIN;
        $user = User::where('email', $credentials['email'])
            ->where('role_id', $credentials['role_id'])
            ->first();
        if ($user && Hash::check($credentials['password'], $user->password)) {
            Auth::login($user);
            return redirect()->intended(route('admin.get_home'));
        }
        return redirect('admin/auth/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect(url('admin/auth/login'));
    }
}