<?php

namespace BackEnd\Admin\Http\Controllers;


use App\SubProduct;
use App\Product;
use BackEnd\Admin\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Laracasts\Flash\Flash;
use yajra\Datatables\Datatables;

class SubProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//

//        $products=Product::where('status',SubProduct::ACTIVE)->lists('category','id')->toArray();
//        return $this->view('pages.subproduct.index')
//            ->with('product',$products);

        $subproduct=SubProduct::$category_id;
        return $this->view('pages.subproduct.index')
            ->with('product',$subproduct);
    }

    public function getsubProductInfo()
    {
        $subproduct = SubProduct::get();
        return Datatables::of($subproduct)
            ->addColumn('show', function ($subproduct) {
                if ($subproduct['status'] == SubProduct::ACTIVE) {
                    return '<div class="btn-group btn-group-sm"><a href="' . url('admin/subproduct/' . $subproduct->id) . ' " class="btn btn-default btn-sm" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a>
                <a href="#" class="btn btn-default btn-sm alert_delete delete-subproduct" subproduct-id="' . $subproduct->id . '".data-toggle="tooltip" title="Delete"> <i class="fa fa-trash-o"></i></a>
                 <a href="' . url('admin/subproduct/deactivate/' . $subproduct->id) . '" class="label btn btn-default btn-sm" data-toggle="tooltip" title="DeActivate"><i class="fa fa-ban"></i></a>
                 </div>';

                } else {
                    return '<div class="btn-group btn-group-sm"><a href="' . url('admin/subproduct/' . $subproduct->id) . ' " class="btn btn-default btn-sm" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a>
                 <a href="#" class="btn btn-default btn-sm alert_delete delete-subproduct" subproduct-id="' . $subproduct->id . '".data-toggle="tooltip" title="Delete"> <i class="fa fa-trash-o"></i></a>
                 <a href="' . url('admin/subproduct/activate/' . $subproduct->id) . '" class="btn btn-default btn-sm" data-toggle="tooltip" title="Activate"> <i class="fa fa-check-circle"></i></a></div>';
                }
            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $validator=Validator::make($input,SubProduct::$rule);
        if($validator->fails()){
            Flash::error('Whoops! Form validation failed');
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }
        $product=new SubProduct();
        $product->category_id=$input['category_id'];
        $product->name=$input['name'];
        $product->description=$input['description'];
        $product->slug = SubProduct::slugMaker($input['name']);
        if ($request->hasFile('image')) {

            $destination_path = 'uploads/subproduct_images/';
            $image = $input['image'];
            $filename = sha1(time() . time()) . preg_replace('/\s+/', '', $image->getClientOriginalName()); //to trim whitespace in file na

            $image->move($destination_path, $filename);

            $product->image = $destination_path . $filename;

        }

        $product->status=SubProduct::ACTIVE;
        $product->save();

        Flash::success('Added new subproduct ..');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//
//        $product=Product::where('status',Product::ACTIVE)
//            ->lists('category','id')
//            ->toArray();
//        $subproduct=SubProduct::find($id);
//
//        return $this->view('pages.subproduct.show')
//            ->with('product_name',$product)
//            ->with('subproduct',$subproduct);

       $product_id=SubProduct::$category_id;;
       $product = SubProduct::find($id);
       if ($product) {
           return $this->view('pages.subproduct.show')
               ->with('subproduct', $product)
               ->with('product_name', $product_id);

       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $validator=Validator::make($input,SubProduct::$rule);
        if($validator->fails()){
            Flash::error('Whoops! Form validation failed');
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }
        $product=SubProduct::find($id);
        $product->category_id=$input['category_id'];
        $product->name=$input['name'];
        $product->description=$input['description'];
        $product->slug = SubProduct::slugMaker($input['name']);
        if ($request->hasFile('image')) {

            $destination_path = 'uploads/subproduct_images/';
            $image = $input['image'];
            $filename = sha1(time() . time()) . preg_replace('/\s+/', '', $image->getClientOriginalName()); //to trim whitespace in file na

            $image->move($destination_path, $filename);

            $product->image = $destination_path . $filename;

        }

        $product->status=SubProduct::ACTIVE;
        $product->save();

        Flash::success('subproduct updated..');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product=SubProduct::find($id);
        if($product){
            $product->delete();
            Flash::success('subproduct Deleted');

            return redirect()->back();
        }else{
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }

    public function activate($id)
    {


        $product = SubProduct::find($id);
        if ($product) {
            $product->status = SubProduct::ACTIVE;
            $product->save();
            Flash::success('subproduct Successfully Activated');
            return redirect()->back();
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }



    public function deactivate($id)
    {
        $product = SubProduct::find($id);
        if ($product) {
            $product->status = SubProduct::DEACTIVATE;
            $product->save();
            Flash::success('subproduct Deactivated');
            return redirect()->back();
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }


}
