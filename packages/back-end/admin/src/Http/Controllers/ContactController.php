<?php

namespace BackEnd\Admin\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;

class ContactController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('pages.contact.index');
    }


    public function getContactInfo()
    {
        $contacts = Contact::get();
        return Datatables::of($contacts)
            ->addColumn('show', function ($contacts) {
                return '<div class="btn-group btn-group-sm"><a href="' . url('admin/contact/' . $contacts->id) . ' " class="btn btn-default btn-sm" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a>
                 <a href="#" class="btn btn-default btn-sm alert_delete delete-contact" contact-id="' . $contacts->id . '".data-toggle="tooltip" title="Delete"> <i class="fa fa-trash-o"></i></a>

                 </div>';


            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);

        return $this->view('pages.contact.show')
            ->with('contact', $contact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        if ($contact) {
            $contact->delete();
            Flash::success('Contact Successfully Deleted');
            return redirect()->to('admin/contact/');
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }

}
