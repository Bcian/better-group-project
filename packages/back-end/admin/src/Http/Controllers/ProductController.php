<?php

namespace BackEnd\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Product;
use App\SubProduct;
use Illuminate\Http\Request;

use App\Http\Requests;
use BackEnd\Admin\Http\Controllers;
use Laracasts\Flash\Flash;
use Validator;
use yajra\Datatables\Datatables;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('pages.product.index');
    }
    public function getProductInfo()
    {
        $product = Product::get();
        return Datatables::of($product)
            ->addColumn('show', function ($product) {
                if ($product['status'] == Product::ACTIVE) {
                    return '<div class="btn-group btn-group-sm"><a href="' . url('admin/product/' . $product->id) . ' " class="btn btn-default btn-sm" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a>
                <a href="#" class="btn btn-default btn-sm alert_delete delete-product" product-id="' . $product->id . '".data-toggle="tooltip" title="Delete"> <i class="fa fa-trash-o"></i></a>
                 <a href="' . url('admin/product/deactivate/' . $product->id) . '" class="label btn btn-default btn-sm" data-toggle="tooltip" title="DeActivate"><i class="fa fa-ban"></i></a>
                 </div>';

                } else {
                    return '<div class="btn-group btn-group-sm"><a href="' . url('admin/product/' . $product->id) . ' " class="btn btn-default btn-sm" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a>
                 <a href="#" class="btn btn-default btn-sm alert_delete delete-product" product-id="' . $product->id . '".data-toggle="tooltip" title="Delete"> <i class="fa fa-trash-o"></i></a>
                 <a href="' . url('admin/product/activate/' . $product->id) . '" class="btn btn-default btn-sm" data-toggle="tooltip" title="Activate"> <i class="fa fa-check-circle"></i></a></div>';
                }
            })->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $validator=Validator::make($input,Product::$rule);
        if($validator->fails()){
            Flash::error('Whoops! Form validation failed');
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }
        $product=new Product();
        $product->category=$input['product'];
        if ($request->hasFile('image')) {

            $destination_path = 'uploads/product_images/';
            $image = $input['image'];
            $filename = sha1(time() . time()) . preg_replace('/\s+/', '', $image->getClientOriginalName()); //to trim whitespace in file na

            $image->move($destination_path, $filename);

            $product->image = $destination_path . $filename;

        }

        $product->status=Product::ACTIVE;
        $product->save();

        Flash::success('Added new product ..');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return $this->view('pages.product.show')
                ->with('product', $product);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $validator=Validator::make($input,Product::$update_rule);
        if($validator->fails()){
            Flash::error('Whoops! Form validation failed');
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }
        $product=Product::find($id);
        $product->category=$input['product'];
        if ($request->hasFile('image')) {

            $destination_path = 'uploads/product_images/';
            $image = $input['image'];
            $filename = sha1(time() . time()) . preg_replace('/\s+/', '', $image->getClientOriginalName()); //to trim whitespace in file na

            $image->move($destination_path, $filename);

            $product->image = $destination_path . $filename;

        }

        $product->status=Product::ACTIVE;
        $product->save();

        Flash::success('product updated ..');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

//        $product=Product::find($id);
//        if($product){
//            $product->delete();
//            Flash::success('product Deleted');
//
//            return redirect()->back();
//        }else{
//            Flash::error('Whoops! Something Went Wrong');
//            return redirect()->back();
//        }

        $product = Product::find($id);
        $subproduct =SubProduct::where('category_id',$id);
        if ($product) {
            $product->delete();
            if($subproduct){
                $subproduct->delete();
            }
            Flash::success('product Successfully Deleted');
            return redirect()->to('admin/product/');
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }

    public function activate($id)
    {


        $product = Product::find($id);
        if ($product) {
            $product->status = Product::ACTIVE;
            $product->save();
            Flash::success('product Successfully Activated');
            return redirect()->back();
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }



    public function deactivate($id)
    {
        $product = Product::find($id);
        if ($product) {
            $product->status = Product::DEACTIVATE;
            $product->save();
            Flash::success('product Deactivated');
            return redirect()->back();
        } else {
            Flash::error('Whoops! Something Went Wrong');
            return redirect()->back();
        }
    }


}
