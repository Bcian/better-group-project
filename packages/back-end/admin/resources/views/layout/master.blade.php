<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title',config('settings.project-name'))</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    {!! HTML::style('back-end/admin/css/bootstrap/css/bootstrap.min.css') !!}
    {!! HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
    {!! HTML::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') !!}
    {!! HTML::style('back-end/admin/dist/css/theme.min.css') !!}
    {!! HTML::style('back-end/admin/dist/css/skins/_all-skins.min.css') !!}
    {!! HTML::style('back-end/admin/plugins/datatables/dataTables.bootstrap.css')!!}
    {!! HTML::script('back-end/admin/plugins/jQuery/jQuery-2.1.4.min.js')!!}
    {!! HTML::script('back-end/admin/plugins/datepicker/datepicker3.css')!!}
    @yield('after-styles-end')
</head>
<body class="skin-blue sidebar-mini" data-base-url={{url()}}>
<div class="wrapper">
    @include('admin::layout.header')
    @include('admin::layout.menu')
    <div class="content-wrapper">
        {{----------FLASH MESSAGE-----------}}
        <div id="flash-message-container" class="flat">
            <div class="row">
                <div class="col-sm-12">
                    @include('vendor.flash.message')
                </div>
            </div>
        </div>
        {{-------FLASH MESSAGE ENDS---------}}
        @yield('content')
    </div>
    @include('admin::layout.footer')
</div>
{!! HTML::script('http://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
{!! HTML::script('back-end/admin/css/bootstrap/js/bootstrap.min.js') !!}
{!! HTML::script('back-end/admin/dist/js/app.min.js') !!}
{!! HTML::script('back-end/admin/plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('back-end/admin/plugins/datatables/dataTables.bootstrap.min.js') !!}
{!! HTML::script('back-end/admin/plugins/datepicker/bootstrap-datepicker.js')!!}
@stack('footer.script')
</body>
</html>