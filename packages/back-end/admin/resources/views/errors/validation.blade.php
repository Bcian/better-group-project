@if (count($errors) > 0)
           <div class="alert alert-danger flash-message alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
           </div>
           @section('alert-message')
            <script type="text/javascript">
               $(document).ready(function()
               {
                $('.flash-message').delay(2000).slideUp();
               });
            </script>
            @stop
        @endif
        @if(Session::has('flash_message'))
        <div class="alert alert-success flash-message alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fa fa-check"></i>  {{ Session::get('flash_message') }}</h5>
        </div>
        @section('alert-message')
        <script type="text/javascript">
           $(document).ready(function()
           {
            $('.flash-message').delay(2000).slideUp();
           });
        </script>
        @stop
        @endif
     @if(Session::has('flash_message_error'))
        <div class="alert alert-error flash-message alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fa fa-check"></i>  {{ Session::get('flash_message_error') }}</h5>
        </div>
        @section('alert-message')
    <script type="text/javascript">
       $(document).ready(function()
       {
        $('.flash-message').delay(2000).slideUp();
       });
    </script>
    @stop
        @endif
