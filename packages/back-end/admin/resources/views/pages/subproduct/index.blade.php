@extends('admin::layout.master')
@section ('title',  env('PROJECT_NAME').'|subproduct Management')
@section('content')
    <section class="content-header">
        <h1 class="edit-form">subproduct  Management</h1>

        <ol class="breadcrumb">
            <li><a href="{{ url("/admin/dashboard") }}"> <i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url("/admin/subproduct") }}">Manage subproduct</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-8">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">subproducts</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover" id="subproduct-info">
                            <thead>
                            <tr>
                                <th>Sl.No.</th>
                                <th>subproduct</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Add subproduct</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('url'=>'/admin/subproduct','files'=>true,'class'=>'form-horizontal-inline')) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'Product Type')!!}
                                    {!!  Form::select('category_id',array('0' => '[Select category]')+$product,null,array('class'=>'form-control input-select-alumni','id'=>'category_id')) !!}
                                    {!! $errors->first('category_id', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'name')!!}
                                    {!! Form::text('name',null,array('class'=>'form-control')) !!}
                                    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'Image')!!}
                                    {!! Form::file('image',null,array('class'=>'form-control')) !!}
                                    {!! Form::label('', 'image_dimensions:1366*699')!!}
                                    {!! $errors->first('image', '<div class="text-danger">This field is required for gallery</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'description')!!}
                                    {!! Form::textarea('description',null,array('class'=>'form-control')) !!}
                                    {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-11">
                                <div class="form-group">
                                    {!! Form::submit("Save", array("class" => "btn bg-primary center-block btn-flat")) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('admin::partials.modal')
@endsection
@push('footer.script')
<script type="text/javascript">
    BASE_URL = $('body').data("base-url");
    $(document).ready(function () {
        /**
         * Get menu info for DataTable
         */
        $('#subproduct-info').DataTable({
            processing: true,
            serverSide: true,
            ajax: BASE_URL + '/admin/subproduct-info',
            columns: [
                {data: 'id', name: 'subproducts.id'},
                {data: 'name', name: 'subproducts.name'},
                {data: 'show', name: 'show', orderable: false, searchable: false}
            ]
        });
        /**
         * Delete popup for menu
         */
        $('#subproduct-info').on('click', '.delete-subproduct', function (event) {
            event.preventDefault();
            var Id = $(this).attr('subproduct-id');
            var destinationPath = BASE_URL + '/admin/subproduct/' + Id;
            $('#delete-confirm').attr('action', destinationPath);
            $("#delete-modal").modal('show');
        });


    });
</script>
@endpush