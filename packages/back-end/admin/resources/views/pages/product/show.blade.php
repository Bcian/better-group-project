@extends('admin::layout.master')
@section ('title',  env('PROJECT_NAME').'|Product Management')
@section('content')
    <section class="content-header">
        <h1 class="edit-form">Product  Management</h1>

        <ol class="breadcrumb">
            <li><a href="{{ url("/admin/dashboard") }}"> <i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url("/admin/product") }}">Manage product Type</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Add product</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => array('admin.product.update',$product->id),'method' => 'PUT','files'=>true)) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'product')!!}
                                    {!! Form::text('product',$product->category,array('class'=>'form-control')) !!}
                                    {!! $errors->first('product', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'Image')!!}
                                    {!! Form::file('image',null,array('class'=>'form-control')) !!}
                                    {!! Form::label('', 'image_dimensions:1366*699')!!}
                                    {!! $errors->first('image', '<div class="text-danger">This field is required for gallery</div>') !!}
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-offset-10 col-sm-2">
                                <div class="form-group">
                                    {!! Form::submit("Update", array("class" => "btn bg-primary center-block btn-flat")) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="add_menu_details" style="display:none">
        <div class="remove-extra-field">

            <div class="col-sm-1">
                <div class="btn-group btn-group-xs">
                    <a href="#" class="btn btn-danger btn-group-sm btn_remove_field_value" id="remove-content-box">
                        <i class="fa fa-minus"></i></a>
                </div>
            </div>


        </div>
    </div>

    @include('admin::partials.modal')
@endsection
