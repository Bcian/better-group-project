@extends('admin::layout.master')
@section ('title',  env('PROJECT_NAME').'|product Management')
@section('content')
    <section class="content-header">
        <h1 class="edit-form">product  Management</h1>

        <ol class="breadcrumb">
            <li><a href="{{ url("/admin/dashboard") }}"> <i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url("/admin/product") }}">Manage product</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-8">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">categories</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover" id="product-info">
                            <thead>
                            <tr>
                                <th>Sl.No.</th>
                                <th>product</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Add product</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('url'=>'/admin/product','files'=>true,'class'=>'form-horizontal-inline')) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'product')!!}
                                    {!! Form::text('product',null,array('class'=>'form-control')) !!}
                                    {!! $errors->first('product', '<div class="text-danger">:message</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('', 'Image')!!}
                                    {!! Form::file('image',null,array('class'=>'form-control')) !!}
                                    {!! Form::label('', 'image_dimensions:1366*699')!!}
                                    {!! $errors->first('image', '<div class="text-danger">This field is required for gallery</div>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-11">
                                <div class="form-group">
                                    {!! Form::submit("Save", array("class" => "btn bg-primary center-block btn-flat")) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('admin::partials.modal')
@endsection
@push('footer.script')
<script type="text/javascript">
    BASE_URL = $('body').data("base-url");
    $(document).ready(function () {
        /**
         * Get menu info for DataTable
         */
        $('#product-info').DataTable({
            processing: true,
            serverSide: true,
            ajax: BASE_URL + '/admin/product-info',
            columns: [
                {data: 'id', name: 'products.id'},
                {data: 'category', name: 'products.category'},
                {data: 'show', name: 'show', orderable: false, searchable: false}
            ]
        });
        /**
         * Delete popup for menu
         */
        $('#product-info').on('click', '.delete-product', function (event) {
            event.preventDefault();
            var Id = $(this).attr('product-id');
            var destinationPath = BASE_URL + '/admin/product/' + Id;
            $('#delete-confirm').attr('action', destinationPath);
            $("#delete-modal").modal('show');
        });


    });
</script>
@endpush