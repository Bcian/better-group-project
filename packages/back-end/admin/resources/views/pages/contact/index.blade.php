@extends('admin::layout.master')
@section ('title',  env('PROJECT_NAME').'|Contacts')
@section('content')
    <section class="content-header">
        <h1 class="edit-form">Contacts</h1>

        <ol class="breadcrumb">
            <li><a href="{{ url("/admin/dashboard") }}"> <i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url("/admin/contacts") }}">Manage Contacts</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Contacts</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover" id="contacts-info">
                            <thead>
                            <tr>
                                <th>Sl.No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('admin::partials.modal')
@endsection
@push('footer.script')
<script type="text/javascript">
    BASE_URL = $('body').data("base-url");
    $(document).ready(function () {


        $('#contacts-info').DataTable({
            processing: true,
            serverSide: true,
            ajax: BASE_URL + '/admin/contacts-info',
            columns: [
                {data: 'id', name: 'contacts.id'},
                {data: 'fname', name: 'contacts.fname'},
                {data: 'email', name: 'contacts.email'},
                {data: 'show', name: 'show', orderable: false, searchable: true}
            ]
        });
        /**
         * Delete popup for Package
         */
        $('#contacts-info').on('click', '.delete-contact', function (event) {
            event.preventDefault();
            var contactlId = $(this).attr('contact-id');
            var destinationPath = BASE_URL + '/admin/contacts/' + contactlId;
            $('#delete-confirm').attr('action', destinationPath);
            $("#delete-modal").modal('show');
        });


    });
</script>
@endpush