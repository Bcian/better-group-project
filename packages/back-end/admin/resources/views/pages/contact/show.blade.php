@extends('admin::layout.master')
@section ('title',  env('PROJECT_NAME').'|Contact Management')
@section('content')
    <section class="content-header">
        <h1 class="edit-form">Contact Management</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url("/admin/dashboard") }}"> <i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url("/admin/contact") }}">Manage Contacts</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Add New</h3>
                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-sm-8">


                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'First Name')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->fname}}</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'Last Name')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->lname}}</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'Email')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->email}}</p>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'Phone')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->phone}}</p>


                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'Subject')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->subject}}</p>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('', 'Message')!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <p>{{$contact->message}}</p>

                                        </div>
                                    </div>

                                </div>


                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>










    @include('admin::partials.modal')
@endsection
@push('footer.script')
<script type="text/javascript">
    BASE_URL = $('body').data("base-url");
    $(document).ready(function () {
        /**
         * Get Album info for DataTable
         */
        $('#contacts-info').DataTable({
            processing: true,
            serverSide: true,
            ajax: BASE_URL + '/admin/contacts-info',
            columns: [
                {data: 'id', name: 'contacts.id'},
                {data: 'fname', name: 'contacts.fname'},
                {data: 'lname', name: 'contacts.lname'},
                {data: 'email', name: 'contacts.email'},
                {data: 'phone', name: 'contacts.phone'},
                {data: 'subject', name: 'contacts.subject'},
                {data: 'message', name: 'contacts.message'},
                {data: 'show', name: 'show', orderable: false, searchable: false}
            ]
        });
        /**
         * Delete popup for Subject
         */
        $('#contacts-info').on('click', '.delete-contacts', function (event) {
            event.preventDefault();
            var serviceId = $(this).attr('contact-id');
            var destinationPath = BASE_URL + '/admin/contact/' + serviceId;
            $('#delete-confirm').attr('action', destinationPath);
            $("#delete-modal").modal('show');
        });


    });
</script>
@endpush